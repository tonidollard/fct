import PageManager from '../PageManager';
import ScrollLink from 'bc-scroll-link';
import FormValidator from './utils/FormValidator';
import Dropdown from './global/Dropdown';
import Header from './global/Header';
import MiniCart from './global/MiniCart';
import QuickShop from './product/QuickShop';
import MegaNav from './global/MegaNav';
import MobileNav from './global/MobileNav';

export default class Global extends PageManager {
    constructor() {
      super();

      new Dropdown($('.dropdown'));
      new Header($('.site-header'));
      new ScrollLink({
        selector: '.button-top'
      });
      new MegaNav($('.mega-nav-variant-container'));
      new MobileNav();

      this._toggleScrollLink();

      new MiniCart();
    }

    /**
     * You can wrap the execution in this method with an asynchronous function map using the async library
     * if your global modules need async callback handling.
     * @param next
     */
    loaded(next) {
      // global form validation
      this.validator = new FormValidator(this.context);
      this.validator.initGlobal();

      // QuickShop
      if ($('[data-quick-shop]').length) {
        new QuickShop(this.context);
      }

      next();
    }

    _toggleScrollLink() {
      $(window).on('scroll', (e) => {
        const winScrollTop = $(e.currentTarget).scrollTop();
        const winHeight = $(window).height();

        if (winScrollTop > winHeight) {
          $('.button-top').addClass('show');
        } else {
          $('.button-top').removeClass('show');
        }
      });
    }
}
